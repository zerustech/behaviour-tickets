ZerusTech Behaviour Tickets
================================================
The *ZerusTech Behaviour Tickets* project provides image and template files that
can be used to print behaviour management tickets for kids. I guess there is no
need to explain why I created this project, so hopefully, it will also be helpful
to those who have been driven crazy by their kids :P

Good Luck !

The Tickets
-------------

This project provides the following tickets:
* 5 Minutes Game Pass
* 10 Minutes Game Pass
* 30 Minutes Game Pass
* 5 Minutes Cartoon Pass
* 10 Minutes Cartoon Pass
* 30 Minutes Cartoon Pass
* 1 Departure Bedtime Pass

### Example

<img src="png/game-pass-10-minutes.png" alt="10 Minutes Game Pass" width="500" />

How to Print?
--------------

You can find templates for printing various tickets in a 99.1mm x 38.1mm label
sheet in the ``pdf/mm-99.1x38.1`` folder, or you can print a single ticket by
using the images in the ``png`` folder.

::: info-box note

When printing a label template file, make sure not to scale the template and
to print it at the original size.

:::

How to Adapt?
--------------

The ``Adobe Illustrator`` source files can be found in the ``src`` folder.


License
-------
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
